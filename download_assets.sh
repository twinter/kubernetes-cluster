ASSET_DIR="./storage/assets"

mkdir -p $ASSET_DIR
cd $ASSET_DIR

echo "downloadings CNI plugins"
CNI_VERSION="v0.8.2"
curl -L# "https://github.com/containernetworking/plugins/releases/download/${CNI_VERSION}/cni-plugins-linux-amd64-${CNI_VERSION}.tgz" > cni.tgz

echo "downloadings crictl"
CRICTL_VERSION="v1.16.0"
curl -L# "https://github.com/kubernetes-sigs/cri-tools/releases/download/${CRICTL_VERSION}/crictl-${CRICTL_VERSION}-linux-amd64.tar.gz" > crictl.tgz

echo "downloadings kubeadm, kubelet and kubectl"
RELEASE="$(curl -sSL https://dl.k8s.io/release/stable.txt)"
curl -L# --remote-name-all https://storage.googleapis.com/kubernetes-release/release/${RELEASE}/bin/linux/amd64/{kubeadm,kubelet,kubectl}

echo "downloading kubelet service file"
curl -L# "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/kubelet.service" | sed "s:/usr/bin:/opt/bin:g" > kubelet.service
curl -L# "https://raw.githubusercontent.com/kubernetes/kubernetes/${RELEASE}/build/debs/10-kubeadm.conf" | sed "s:/usr/bin:/opt/bin:g" > 10-kubeadm.conf