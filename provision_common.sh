ASSET_DIR="/vagrant/assets"



echo "installing CNI plugins"
mkdir -p /opt/cni/bin
tar -f $ASSET_DIR/cni.tgz -C /opt/cni/bin -xz



echo "installing crictl"
mkdir -p /opt/bin
tar -f $ASSET_DIR/crictl.tgz -C /opt/bin -xz



echo "installing kubeadm, kubelet and kubectl"
mkdir -p /opt/bin
cd /opt/bin
cp $ASSET_DIR/{kubeadm,kubelet,kubectl} .
chmod +x {kubeadm,kubelet,kubectl}

cp $ASSET_DIR/kubelet.service /etc/systemd/system/kubelet.service
mkdir -p /etc/systemd/system/kubelet.service.d
cp $ASSET_DIR/10-kubeadm.conf /etc/systemd/system/kubelet.service.d/10-kubeadm.conf



echo "enable and start kubelet"
systemctl enable --now kubelet



echo "enable and start docker"
systemctl enable --now docker



echo "change docker cgroup driver to systemd and restart docker to apply it"
cat <<EOF >/etc/docker/daemon.json
{
  "exec-opts": ["native.cgroupdriver=systemd"],
  "log-driver": "json-file",
  "log-opts": {
    "max-size": "100m"
  },
  "storage-driver": "overlay2"
}
EOF
systemctl restart docker



echo "making sure k8s uses the correct ip"
IP=$(ip addr | grep inet | grep enp0s8 | awk -F" " '{print $2}'| sed -e 's/\/.*$//')
# the next segment was taken from https://github.com/oracle/vagrant-boxes/blob/master/Kubernetes/scripts/provision.sh (licensed under the UPLv1.0) on 23.09.2019
KubeletNode="/etc/systemd/system/kubelet.service.d/90-node-ip.conf"
ExecStart=$(grep ExecStart=/ /etc/systemd/system/kubelet.service.d/10-kubeadm.conf | sed -e 's/\$KUBELET_EXTRA_ARGS/\$KUBELET_EXTRA_ARGS \$KUBELET_NODE_IP_ARGS/')
cat <<-EOF >${KubeletNode}
	[Service]
	Environment="KUBELET_NODE_IP_ARGS=--node-ip=${IP}"
	ExecStart=
	${ExecStart}
EOF
chmod 644 ${KubeletNode}
systemctl daemon-reload

# restart the kubelet with the changed settings
systemctl restart kubelet



# ensure iptables tooling does not use the nftables backend, not required on all system
#update-alternatives --set iptables /usr/sbin/iptables-legacy
#update-alternatives --set ip6tables /usr/sbin/ip6tables-legacy
#update-alternatives --set arptables /usr/sbin/arptables-legacy
#update-alternatives --set ebtables /usr/sbin/ebtables-legacy
