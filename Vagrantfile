# -*- mode: ruby -*-
# vi: set ft=ruby :


#
# You have to run ./download_assets.sh to download the assets required when provisioning
#


# amount of workers (up to 249)
WORKER_AMOUNT = 1

# amount of RAM for each VM in MiB
MEMORY_MASTER = "2048"
MEMORY_WORKER = "5120"  # 5 GiB

# cpu cores per VM
CPU_CORES_MASTER = 2
CPU_CORES_WORKER = 2



Vagrant.configure("2") do |config|
  config.vm.box = "amam/coreos-stable"  # CoreOS image with working shared folders

  # SHARED FOLDERS
  # disable default shared folder
  config.vm.synced_folder ".", "/vagrant", disabled: true
  # add new shared folder to all VMs for dynamically generated config data
  config.vm.synced_folder "./storage/shared", "/vagrant/shared", create: true
  # folder for previously downloaded assets to speed up provisioning
  config.vm.synced_folder "./storage/assets", "/vagrant/assets"

  # define master VM
  config.vm.define "master", primary: true do |master|
  	master.vm.hostname = "master.vagrant.vm"

    master.vm.provision "shell", path: "./provision_common.sh"
    master.vm.provision "shell", path: "./provision_master.sh"
    
    master.vm.network "private_network", ip: "10.1.0.250"
    master.vm.network "public_network", bridge: "enp2s0"

    master.vm.provider "virtualbox" do |vb|
      vb.memory = MEMORY_MASTER
      vb.cpus = CPU_CORES_MASTER
    end

    # add synced folder with config files (as root to prevent accidental modification)
    master.vm.synced_folder "./config", "/vagrant/config", owner: "root", group: "root"
  end

  # define workers
  (1..WORKER_AMOUNT).each do |i|
    config.vm.define "worker#{i}" do |worker|
      worker.vm.hostname = "worker#{i}.vagrant.vm"

      worker.vm.provision "shell", path: "./provision_common.sh"
      worker.vm.provision "shell", path: "./provision_worker.sh"
      
      worker.vm.network "private_network", ip: "10.1.0.#{i}"

      worker.vm.provider "virtualbox" do |vb|
        vb.memory = MEMORY_WORKER
        vb.cpus = CPU_CORES_WORKER
      end
      
      # add synced folder for permanent storage
      worker.vm.synced_folder "./storage/worker#{i}", "/vagrant/worker", create: true
    end
  end

end
