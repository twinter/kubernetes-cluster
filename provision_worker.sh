# import join token and certificate hash
echo "importing join token data"
TOKEN=$(cat /vagrant/shared/join-token.txt)
HASH=$(cat /vagrant/shared/join-hash.txt)

# join the cluster
kubeadm join --token $TOKEN 10.1.0.250:6443 --discovery-token-ca-cert-hash sha256:$HASH
