# home folde
HOME_DIR="/home/core"



# echo "add autocompletion for kubeadm and kubectl"
# # replace symlink to .bashrc with the actual file
# cp $(readlink .bashrc) .bashrc.new && mv .bashrc.new .bashrc

# # add kubectl and kubeadm autocompletion to bash
# echo 'source <(kubectl completion bash)' >>$HOME_DIR/.bashrc
# echo 'source <(kubeadm completion bash)' >>$HOME_DIR/.bashrc



echo "make kubectl executable to the default user"
mkdir -p $HOME_DIR/.kube
cp -i /etc/kubernetes/admin.conf $HOME_DIR/.kube/config
chown core:core $HOME_DIR/.kube/config



echo "kubeadm init"
# init kubernetes with kube-proxy network settings
kubeadm init --pod-network-cidr=10.244.0.0/16 --apiserver-advertise-address=10.1.0.250



echo "exporting join token and certificate hash"
kubeadm token create > /vagrant/shared/join-token.txt
openssl x509 -pubkey -in /etc/kubernetes/pki/ca.crt | openssl rsa -pubin -outform der 2>/dev/null | openssl dgst -sha256 -hex | sed 's/^.* //' > /vagrant/shared/join-hash.txt



# set shortcut for kubeconfig
KUBECONFIG=$HOME_DIR/.kube/config



echo "installing kube-router"
kubectl --kubeconfig=$KUBECONFIG apply -f https://raw.githubusercontent.com/cloudnativelabs/kube-router/master/daemonset/kubeadm-kuberouter.yaml



echo "install ceph cluster and block storage class"
kubectl --kubeconfig=$KUBECONFIG apply -f /vagrant/config/rook-ceph-0-common.yaml
kubectl --kubeconfig=$KUBECONFIG apply -f /vagrant/config/rook-ceph-1-operator.yaml
kubectl --kubeconfig=$KUBECONFIG apply -f /vagrant/config/rook-ceph-2-cluster.yaml
kubectl --kubeconfig=$KUBECONFIG apply -f /vagrant/config/rook-ceph-3-storageclass-block.yaml

# TODO: install kubeflow
